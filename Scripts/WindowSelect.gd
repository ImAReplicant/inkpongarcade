extends Node2D

onready var labelRed = $MarginContainer/VBoxContainer/HBoxContainer/Red
onready var labelGreen = $MarginContainer/VBoxContainer/HBoxContainer/Green
onready var labelBlue = $MarginContainer/VBoxContainer/HBoxContainer/Blue
onready var labelYellow = $MarginContainer/VBoxContainer/HBoxContainer/Yellow

onready var paddleRed = $PaddleRed/CPUParticles2D
onready var paddleGreen = $PaddleGreen/CPUParticles2D
onready var paddleBlue = $PaddleBlue/CPUParticles2D
onready var paddleYellow = $PaddleYellow/CPUParticles2D

onready var settingsScene = $WindowGameSettings
onready var stageButton = $WindowGameSettings/MarginContainer/VBoxContainer/VBoxContainer/HBoxStage/Stage
onready var startButton = $MarginContainer/VBoxContainer/HBoxStart_Settings/Settings

onready var gameScene = preload("res://Scenes/WindowGame.tscn")
onready var menuScene = load("res://Scenes/WindowMenu.tscn")

onready var tableLabel = [labelRed, labelGreen, labelBlue, labelYellow]

var timer = 0

var leftP1
var rightP1
var spaceP1
var cancelP1

var leftP2
var rightP2
var spaceP2
var cancelP2

var focus = false
var inhibitInput = false
var selectedP1 = false
var selectedP2 = false
var selectedP1P2 = false
var textP1 = "^\np1"
var textP2 = "^\np2"

var white = Color("fff")


func _ready() -> void:
	labelRed.text = textP1
	labelGreen.text = ""
	if Global.game == "P2":
		labelBlue.text = textP2
	else:
		labelBlue.text = ""
	labelYellow.text = ""


func _process(_delta) -> void:
	if inhibitInput == false:
		get_input()
		if !selectedP1:
			choose_color(textP1, rightP1, leftP1)
		if Global.game == "P2":
			if !selectedP2:
				choose_color(textP2, rightP2, leftP2)
		select_color()
		
	if focus and (Input.is_action_just_pressed("ui_cancel") or  Input.is_action_just_pressed("ui_cancel_p2")):
		startButton.release_focus()
		inhibitInput = false
		cancel_selection(textP1)
		cancel_selection(textP2)


func get_input() -> void:
	leftP1 = Input.is_action_just_pressed("ui_left")
	rightP1 = Input.is_action_just_pressed("ui_right")
	spaceP1 = Input.is_action_just_pressed("ui_select")
	cancelP1 = Input.is_action_just_pressed("ui_cancel")
	
	if Global.game == "P2":
		leftP2 = Input.is_action_just_pressed("ui_left_p2")
		rightP2 = Input.is_action_just_pressed("ui_right_p2")
		spaceP2 = Input.is_action_just_pressed("ui_select_p2")
		cancelP2 = Input.is_action_just_pressed("ui_cancel_p2")


func choose_color(text:String, right:bool, left:bool) -> void:
	var text2
	if text == textP1:
		text2 = textP2
	else:
		text2 = textP1
	for i in tableLabel.size():
		if tableLabel[i].text == text:
			if right:
				tableLabel[i].text = ""
				if i == 3:
					if tableLabel[0].text != text2:
						tableLabel[0].text = text
					else:
						tableLabel[1].text = text
				else:
					if tableLabel[i+1].text != text2:
						tableLabel[i+1].text = text
					else:
						if i == 2:
							tableLabel[0].text = text
						else:
							tableLabel[i+2].text = text
			if left:
				tableLabel[i].text = ""
				if i == 0:
					if tableLabel[3].text != text2:
						tableLabel[3].text = text
					else:
						tableLabel[2].text = text
				else:
					if tableLabel[i-1].text != text2:
						tableLabel[i-1].text = text
					else:
						tableLabel[i-2].text = text
			break


func select_color() -> void:
	if cancelP1:
		if !selectedP1 and !selectedP2:
			quit_selection()
		cancel_selection(textP1)

	if spaceP1:
		if !selectedP1:
			get_selection(textP1)

	if Global.game == "P2":
		if cancelP2:
			if !selectedP1 and !selectedP2:
				quit_selection()
			cancel_selection(textP2)

		if spaceP2:
			if !selectedP2:
				get_selection(textP2)

	if selectedP1 and selectedP2:
		if spaceP1 or spaceP2:
			inhibitInput = true
			startButton.grab_focus()


func get_selection(player:String) -> void:
	timer = 0
	for label in tableLabel:
		if label.text == player:
			if player == textP1:
				selectedP1 = true
				Global.P1Color = label.name
				if Global.game == "CPU":
					selectedP2 = true
					if label.name == "Red":
						Global.P2Color = "Green"
					if label.name == "Green":
						Global.P2Color = "Blue"
					if label.name == "Blue":
						Global.P2Color = "Yellow"
					if label.name == "Yellow":
						Global.P2Color = "Red"
			else:
				selectedP2 = true
				Global.P2Color = label.name

			if label.name == "Red":
				label.modulate = Color("a6424c")
				paddleRed.emitting = true
			if label.name == "Green":
				label.modulate = Color("99e550")
				paddleGreen.emitting = true
			if label.name == "Blue":
				label.modulate = Color("507dcc")
				paddleBlue.emitting = true
			if label.name == "Yellow":
				label.modulate = Color("fffb00")
				paddleYellow.emitting = true


func cancel_selection(player:String) -> void:
	if player == textP1:
		selectedP1 = false
		if Global.game == "CPU":
			selectedP2 = false
	else:
		selectedP2 = false
	selectedP1P2 = false
	for label in [labelRed, labelGreen, labelBlue, labelYellow]:
		if label.text == player:
			label.modulate = white
			if label.name == "Red":
				paddleRed.emitting = false
			if label.name == "Green":
				paddleGreen.emitting = false
			if label.name == "Blue":
				paddleBlue.emitting = false
			if label.name == "Yellow":
				paddleYellow.emitting = false


func quit_selection() -> void:
	var new = menuScene.instance()
	get_tree().get_root().get_node("Game/CRT/Viewport").add_child(new)
	var VB = get_tree().get_root().get_node("Game/CRT/Viewport/WindowMenu/Menu/VBoxContainer")
	VB.get_node("VBoxContainer").visible = false
	VB.get_node("VBoxPlay").visible = true
	if Global.game == "P2":
		VB.get_node("VBoxPlay/2P").grab_focus()
	else:
		VB.get_node("VBoxPlay/1P").grab_focus()
	Global.currentScene = "Menu"
	queue_free()


func new_scene(scene) -> void:
	var new = scene.instance()
	get_tree().get_root().get_node("Game/CRT/Viewport").add_child(new)
	queue_free()


func _on_Settings_pressed():
	settingsScene.visible = true
	stageButton.grab_focus()


func _on_Start_pressed():
	new_scene(gameScene)
	Global.currentScene = "Game"


func _on_Settings_focus_entered():
	focus = true


func _on_Settings_focus_exited():
	focus = false


func _on_Start_focus_entered():
	focus = true


func _on_Start_focus_exited():
	focus = false
