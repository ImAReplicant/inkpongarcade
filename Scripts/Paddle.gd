extends KinematicBody2D

signal power_shot

export(String) var color = "Red"

const SPEED = 200
var direction = Vector2()
var lockPositionX = 0
var maxLiquid = 0.80
var minLiquid = 0.43
var liquidFill = maxLiquid
var liquidCostPowerShot = 0.1

var down
var up
var reload
var powerShot = 0

onready var powerShotParticles = $PowerShotParticles
onready var powerShotSound = $PowerShotSound
onready var timer = $Timer
onready var liquid = $Sprite/Liquid
onready var ball = get_node("../Ball")

onready var tree = get_tree().get_root().get_node("Game")
onready var lightLeft = tree.get_node("Light2DLeft").get_node("AnimationPlayer")

func _ready() -> void:
	# Locks the X position to prevent shifting when the ball hits the paddle
	lockPositionX = position.x
	select_color(Global.P1Color)


func select_color(new_color) -> void:
	if new_color == "Red":
		liquid.material.set_shader_param("Color", Color("ff0000"))
		$CPUParticles2D.color = Color("a6424c")
		powerShotParticles.color = Color("a6424c")
	elif new_color == "Green":
		liquid.material.set_shader_param("Color", Color("00ff04"))
		$CPUParticles2D.color = Color("99e550")
		powerShotParticles.color = Color("99e550")
	elif new_color == "Blue":
		liquid.material.set_shader_param("Color", Color("0040ff"))
		$CPUParticles2D.color = Color("507dcc")
		powerShotParticles.color = Color("507dcc")
	elif new_color == "Yellow":
		liquid.material.set_shader_param("Color", Color("fffb00"))
		$CPUParticles2D.color = Color("eec39a")
		powerShotParticles.color = Color("eec39a")


func get_input() -> void:
	down = Input.get_action_strength("ui_down")
	up = Input.get_action_strength("ui_up")
	reload = Input.is_action_pressed("ui_select")
	powerShot = Input.is_action_just_pressed("ui_select")
	
	if liquidFill <= minLiquid:
		direction = Vector2.ZERO
	else:
		direction = Vector2(0, down - up)
	if direction == Vector2.ZERO:
		$CPUParticles2D.emitting = false
	else:
		$CPUParticles2D.emitting = true
	
	if reload:
		liquidFill += 0.001
		direction = Vector2.ZERO


func _physics_process(delta) -> void:
	get_input()
	position.x = lockPositionX
	var collision = move_and_collide(SPEED * direction * delta)
	
	if collision:
		if collision.collider.name == "Ball":
			collision()

	liquidFill = liquid_fill(collision)
	position.y = round(position.y)

	if timer.time_left > 0:
		$Sprite.modulate = Color(10,10,10,10)
		
		if powerShot:
			if liquidFill >= minLiquid + liquidCostPowerShot:
				powerShot = 0
				lightLeft.stop()
				get_tree().paused = true
				powerShotSound.play()
				powerShotParticles.emitting = true
				liquidFill -= liquidCostPowerShot
				emit_signal("power_shot")


func collision() -> void:
	lightLeft.play("Blink")
	
	if timer.is_stopped():
		timer.start(0.090)


func liquid_fill(col) -> float:
	if direction != Vector2.ZERO and !col:
		liquidFill -= 0.001
	liquid.material.set_shader_param("Fill", liquidFill)
	return clamp(liquidFill, minLiquid, maxLiquid)


func _on_Timer_timeout():
	get_tree().paused = false
	$Sprite.modulate = Color(1,1,1,1)
