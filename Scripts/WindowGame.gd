extends Node2D

onready var resultScene = preload("res://Scenes/WindowResult.tscn")
onready var paddle1 = $GameObjects/PaddleP1/CPUParticles2D
onready var paddle2 = $GameObjects/PaddleP2/CPUParticles2D
onready var pauseScreen = $PauseScreen
onready var menuScene = load("res://Scenes/WindowMenu.tscn")

var stage = Global.stage


func _ready() -> void:
	Global.connect("game_is_finished", self, "game_finished")
	select_stage(stage)
	Global.bonusP1 = 0.0
	Global.bonusP2 = 0.0


func _process(_delta) -> void:
	var pause = get_input()
	if pause:
		pause_screen()


func game_finished() -> void:
	var new = resultScene.instance()
	get_tree().get_root().get_node("Game/CRT/Viewport").add_child(new)
	Global.currentScene = "Menu"
	
	var childrenStage = get_tree().get_nodes_in_group("stage")
	for child in childrenStage:
		if stage == 4:
			if child.get_node("ColorRect").color == paddle1.color:
				Global.bonusP1 += 10.0
			elif child.get_node("ColorRect").color == paddle2.color:
				Global.bonusP2 += 10.0
		else:
			if child.get_node("ColorRect").color == paddle1.color:
				Global.bonusP1 += 5.0
			elif child.get_node("ColorRect").color == paddle2.color:
				Global.bonusP2 += 5.0


func select_stage(s) -> void:
	var childrenStage = get_tree().get_nodes_in_group("stage")
	for child in childrenStage:
		if s == 2:
			if child.name == "Stage2_1" or child.name == "Stage2_2":
				child.get_node("CollisionShape2D").disabled = false
				child.visible = true
		elif s == 3:
			if child.name == "Stage3_1" or child.name == "Stage3_2":
				child.get_node("CollisionShape2D").disabled = false
				child.visible = true
		elif s == 4:
			if child.name == "Stage4":
				child.get_node("CollisionShape2D").disabled = false
				child.visible = true


func pause_screen() -> void:
	pauseScreen.visible = true
	pauseScreen.get_node("MarginContainer/VBoxContainer/HBoxContainer/Resume").grab_focus()
	get_tree().paused = true
	var paddles = get_tree().get_nodes_in_group("paddle")
	for paddle in paddles:
		paddle.pause_mode = Node.PAUSE_MODE_STOP


func get_input() -> bool:
	var pause = Input.is_action_just_pressed("ui_global_cancel")
	return pause


func _on_Resume_pressed() -> void:
	pauseScreen.visible = false
	get_tree().paused = false
	var paddles = get_tree().get_nodes_in_group("paddle")
	for paddle in paddles:
		paddle.pause_mode = Node.PAUSE_MODE_PROCESS


func _on_Quit_pressed() -> void:
	get_tree().paused = false
	var paddles = get_tree().get_nodes_in_group("paddle")
	for paddle in paddles:
		paddle.pause_mode = Node.PAUSE_MODE_PROCESS
	var new = menuScene.instance()
	get_tree().get_root().get_node("Game/CRT/Viewport").add_child(new)
	Global.currentScene = "Menu"
	queue_free()
