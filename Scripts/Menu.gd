extends MarginContainer

onready var selectScene = preload("res://Scenes/WindowSelect.tscn")

onready var menu = $VBoxContainer/VBoxContainer
onready var option = $VBoxContainer/VBoxOptions
onready var play = $VBoxContainer/VBoxPlay

onready var crt = $VBoxContainer/VBoxOptions/CRT
onready var music = $VBoxContainer/VBoxOptions/Music


func _ready() -> void:
	menu.get_node("Play").grab_focus()
	crt.pressed = Global.CRT


func _process(_delta) -> void:
	crt.pressed = Global.CRT
	music.pressed = Global.Music


func start_game() -> void:
	var new = selectScene.instance()
	get_tree().get_root().get_node("Game/CRT/Viewport").add_child(new)
	get_parent().queue_free()


# Main Menu
func _on_Options_pressed():
	option.visible = true
	menu.visible = false
	option.get_node("Music").grab_focus()

func _on_Play_pressed():
	play.visible = true
	menu.visible = false
	play.get_node("1P").grab_focus()

func _on_Quit_pressed():
	get_tree().quit()


# Options Menu
func _on_Azerty_pressed():
	Global.azertyKeyboard = true

func _on_Qwerty_pressed():
	Global.azertyKeyboard = false

func _on_CRT_pressed():
	Global.CRT = !Global.CRT

func _on_Music_pressed():
	Global.Music = !Global.Music

func _on_Back_pressed():
	option.visible = false
	play.visible = false
	menu.visible = true
	menu.get_node("Play").grab_focus()


# Play Menu
func _on_1P_pressed():
	Global.game = "CPU"
	start_game()

func _on_2P_pressed():
	Global.game = "P2"
	start_game()
