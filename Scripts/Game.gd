extends Control

onready var timer = $CRT/Viewport/Timer
onready var timerLabel = $CRT/Viewport/Timer/Label
onready var startLabel = $CRT/Viewport/StartLabel
onready var endSound = $EndSound
onready var coinSound = $CoinSound
onready var musicStream = $Music
onready var music1 = load("res://Music/Chippy Music 1.wav")
onready var music2 = load("res://Music/Chippy Music 2.wav")
onready var music3 = load("res://Music/Chippy Music 3.wav")
onready var CRTnode = $CRT
onready var CRTshader = preload("res://Shaders/CRT.tres")
onready var lightP1 = $Light2DLeft
onready var lightP2 = $Light2DRight

var timerTime
var gameStarted = false


func _process(_delta) -> void:
	CRT()
	get_input()
	if Global.currentScene == "Menu":
		startLabel.visible = false
		timerLabel.visible = false
		gameStarted = false
		timer.stop()
		musicStream.stop()
	if gameStarted:
		if float(timerLabel.text) < 5.0:
			timerLabel.get_node("AnimationPlayer").play("blink")
			timerLabel.modulate = Color("d95763")
			var timerLabelStr = "%.1f" % timer.time_left
			timerLabel.text = timerLabelStr
		else:
			var roundTimer = round(timer.time_left)
			timerLabel.text = str(roundTimer)
	else:
		if Global.currentScene == "Game":
			startLabel.text = "Press start"
			startLabel.visible = true


func get_input() -> void:
	if gameStarted == false and Global.currentScene == "Game":
		if Input.is_action_just_pressed("ui_select"):
			game_started()
	if Global.currentScene == "Menu":
		if Input.is_action_just_pressed("ui_select") or Input.is_action_just_pressed("ui_select_p2"):
			coinSound.play()


func CRT() -> void:
	if OS.get_name() == "HTML5":
		if Global.CRT == true:
			get_node("CRTWeb").visible = true
		elif Global.CRT == false:
			get_node("CRTWeb").visible = false
	else:
		if Global.CRT == true:
			if CRTnode.material == null:
				CRTnode.set_material(CRTshader)
		elif Global.CRT == false:
			if CRTnode.material != null:
				CRTnode.material = null


func game_started() -> void:
	startLabel.visible = false
	timerLabel.visible = true
	timerTime = Global.timer
	timer.start(timerTime)
	gameStarted = true
	select_color(lightP1, Global.P1Color)
	select_color(lightP2, Global.P2Color)
	var music = Global.musicPlay
	if Global.Music:
		if music == 1 and musicStream.stream != music1:
			musicStream.stream = music1
		elif music == 2 and musicStream.stream != music2:
			musicStream.stream = music2
		elif music == 3 and musicStream.stream != music3:
			musicStream.stream = music3
		musicStream.play()
	Global.emit_signal("game_is_started")


func select_color(light, new_color) -> void:
	if new_color == "Red":
		light.color = Color("be0000")
	elif new_color == "Green":
		light.color = Color("00be04")
	elif new_color == "Blue":
		light.color = Color("0046ff")
	elif new_color == "Yellow":
		light.color = Color("bebe00")


func _on_Timer_timeout() -> void:
	gameStarted = false
	timerLabel.get_node("AnimationPlayer").stop()
	endSound.play()
	musicStream.stop()
	timerLabel.modulate = Color(1,1,1)
	timerLabel.visible = false
	startLabel.visible = false
	timerLabel.text = str(timerTime)
	Global.emit_signal("game_is_finished")
