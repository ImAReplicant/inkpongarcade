# Table of Contents

-   [Contrôles :](#orgfc29462)
-   [Version POST-JAM](#org90f261e)
    -   [Correctifs :](#org33f834b)
    -   [Ajouts :](#org208cbac)
-   [Crédit :](#org0f4e8d9)

Page Itch.io : <https://imareplicant.itch.io/inkpongarcade>

Jeu réalisé lors de la VIDEO GAMES LAB JAM 2021 et développé sur GODOT. Merci à At0mium pour avoir organisé cet JAM.

C'est le premier jeux que je termine vraiment, j'espère qu'il vous plaira.

Le jeux est un mélange entre PONG et SPLATOON, le but est de peindre plus de terrain que l'adversaire.

Les raquettes sont des réservoirs d'encres qui se vide lors du déplacement.
Une fois le réservoir vide impossible de se déplacer ! 
Recharger le réservoir empêche le déplacement.
Il est possible de faire un tir puissant qui consomme de l'encre en appuyant sur espace quand la balle touche la raquette.

L'effet télévision cathodique visible sur les screenshots n'est pas disponible sur la version WEB

---


<a id="orgfc29462"></a>

# Contrôles :

-   Joueur 1 :
    -   ZQSD
    -   Espace (maintenir pour recharger, tire puissant)
    -   Echape (Pause, retour dans les menus)
-   Joueur 2:
    -   Flèches directionnelles ou 8456 pavé numérique
    -   Entrer (maintenir pour recharger, tire puissant)
    -   Suppr (Pause, retour dans les menus


<a id="org90f261e"></a>

# Version POST-JAM


<a id="org33f834b"></a>

## Correctifs :

-   Grosse optimisation en modifiant juste un paramètre. Merci à l'entretien de [Raffaele Picca](https://www.youtube.com/watch?v=cwZGq1qJYoQ)
-   Modification des valeurs min/max du réservoir de peinture
-   La balle était lancée automatiquement lors d'une deuxième partie
-   Le compteur continuai à clignoter en rouge lors d'une deuxième partie
-   Parfois la balle ne changeai pas de couleur lors d'une collision


<a id="org208cbac"></a>

## Ajouts :

-   Feedback pour le tir puissant (particules, son)
-   Animation lors du choix des raquettes
-   Le choix clavier azerty/qwerty est maintenant automatique
-   La balle va aléatoirement vers le joueurs 1 ou le joueur 2 en début
    de partie
-   Paramètre de partie pour choisir:
    -   la musique (2 nouvelles musique ajoutées)
    -   le temps de jeu (30s à 120s)
    -   le stage (3 nouveaux stage ajoutées)
-   Possibilité de mettre en pause la partie


<a id="org0f4e8d9"></a>

# Crédit :

-   Police : <https://mounirtohami.itch.io/minimalpixel-font>
-   Son : <https://phoenix1291.itch.io/sound-effects-pack-2>
-   Musique : <https://chippy01302.itch.io/chippy-music-pack>
-   Effet télé cathodique :
    -   version PC : <https://godotshaders.com/shader/vhs-and-crt-monitor-effect/>
    -   version Web :
        -   <https://github.com/henriquelalves/SimpleGodotCRTShader>
        -   <https://godotshaders.com/shader/crt-shader/>

