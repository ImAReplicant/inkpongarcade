extends Node

var P1Color:String
var P2Color:String
var game:String = "P2"

var CRT = true
var Music = true
var currentScene = "Menu"

var timer = 60
var musicPlay = 1
var stage = 1

var resultP1 = 0
var resultP2 = 0
var bonusP1 = 0.0
var bonusP2 = 0.0

signal game_is_finished()
signal game_is_started()
