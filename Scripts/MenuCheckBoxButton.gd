tool
extends CheckBox

export(int) var arrow_margin_from_center = 100


func _ready() -> void:
	hide_arrows()
	set_focus_mode(true)


func _process(_delta) -> void:
	if Engine.editor_hint:
		show_arrows()


func show_arrows() -> void:
	for arrow in [$LeftArrow, $RightArrow]:
		arrow.visible = true
		arrow.rect_global_position.y = rect_global_position.y 

	var center_x = rect_global_position.x + (rect_size.x / 2)
	$LeftArrow.rect_global_position.x = center_x - arrow_margin_from_center - $LeftArrow.rect_size.x/2 +2
	$RightArrow.rect_global_position.x = center_x + arrow_margin_from_center - $RightArrow.rect_size.x/2 +1


func hide_arrows() -> void:
	for arrow in [$LeftArrow, $RightArrow]:
		arrow.visible = false


func _on_CheckBox_focus_entered():
	show_arrows()

func _on_CheckBox_focus_exited():
	hide_arrows()

func _on_CheckBox_mouse_entered():
	grab_focus()
