tool
extends TextureButton

export(String) var text = "Text button"
export(int) var arrow_margin_from_center = 100


func _ready() -> void:
	setup_text()
	hide_arrows()
	set_focus_mode(true)


func _process(_delta) -> void:
	if Engine.editor_hint:
		setup_text()
		show_arrows()


func setup_text() -> void:
	$Label.text = text


func show_arrows() -> void:
	for arrow in [$LeftArrow, $RightArrow]:
		if arrow_margin_from_center != 0:
			arrow.visible = true
		else:
			arrow.visible = false

	var center_x = rect_global_position.x + (rect_size.x / 2)
	$LeftArrow.rect_global_position.x = center_x - arrow_margin_from_center - $LeftArrow.rect_size.x/2 +2
	$RightArrow.rect_global_position.x = center_x + arrow_margin_from_center - $RightArrow.rect_size.x/2 +1


func hide_arrows() -> void:
	for arrow in [$LeftArrow, $RightArrow]:
		arrow.visible = false


func _on_TextureButton_focus_entered():
	show_arrows()

func _on_TextureButton_focus_exited():
	hide_arrows()

func _on_TextureButton_mouse_entered():
	grab_focus()
