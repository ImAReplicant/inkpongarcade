extends KinematicBody2D

var speed = 200
export var direction : Vector2
var velocity
var color

var pos1 = Vector2(50, 25)
var pos2 = Vector2(150, 25)
var pos3 = Vector2(150, 125)
var pos4 = Vector2(50, 125)

signal change_color(col)
signal change_position(pos)
onready var paintSound = $PaintSound
onready var collisionSound = $CollisionSound

export var intensity : float
export var duration : float


func _ready() -> void:
	randomize()
	apply_ink_color(Color("ffffff"))
	direction = direction.normalized()
	velocity_ball()
	Global.connect("game_is_finished", self, "game_finished")
	Global.connect("game_is_started", self, "game_started")
	if Global.stage == 4:
		var random = randi() % 4
		if random == 0:
			position = pos1
		if random == 1:
			position = pos2
		if random == 2:
			position = pos3
		if random == 3:
			position = pos4
	else:
		position = Vector2(100, 75)

func game_finished():
	collisionSound.volume_db = -5
	paintSound.volume_db = -40


func game_started():
	if position == pos1:
		direction.x = 1
		direction.y = 1
	elif position == pos2:
		direction.x = -1
		direction.y = 1
	elif position == pos3:
		direction.x = -1
		direction.y = -1
	elif position == pos4:
		direction.x = 1
		direction.y = -1
	else:
		var random = randi() % 2
		if random == 0:
			direction.x = 1
		if random == 1:
			direction.x = -1


func _physics_process(delta) -> void:
	var collision = move_and_collide(velocity * delta)
	
	if speed > 200:
		speed -= 50 * delta
	
	if collision:
		collision_ball(collision)

	velocity_ball()
	emit_signal("change_position", position)


func apply_ink_color(new_color) -> void:
	$ColorRect.color = new_color
	emit_signal("change_color", new_color)


func velocity_ball() -> void:
	velocity = direction * speed	
	velocity = velocity.round()


func collision_ball(col) -> void:
	Shake.shake(intensity, duration)
	collisionSound.play()
	
	if col.collider.is_in_group("paddle"):
		paintSound.play()
		var paddle = col.collider
		# Gives an angle to the ball depending on the position on the paddle
		var difference = position - paddle.position
		direction = difference.normalized()
		return
		
	if col.collider.is_in_group("stage"):
		paintSound.play()
		var stage = col.collider
		if $ColorRect.color != Color(1,1,1,1):
			stage.get_node("ColorRect").color = $ColorRect.color

	direction = direction.bounce(col.normal)


func _on_PaddleP1_power_shot():
	speed = 400


func _on_PaddleP2_power_shot():
	speed = 400


func _on_Area2DBall_area_entered(area):
	var paddle = area.get_parent()
	color = paddle.get_node("CPUParticles2D").color
	if paddle.liquidFill > paddle.minLiquid:
		apply_ink_color(color)
