extends Node2D

onready var liquidP1 = $Sprite/LiquidP1
onready var liquidP2 = $Sprite/LiquidP2
onready var label = $Label
onready var labelP1 = $LabelP1
onready var labelP2 = $LabelP2
onready var menuScene = load("res://Scenes/WindowMenu.tscn")

var resultP1
var resultP2
var resultMin
var resultP1Scale
var resultP2Scale
var pourcentP1
var pourcentP2
var timer = 0

var spaceP1
var spaceP2


func _ready() -> void:
	select_color(liquidP1, Global.P1Color)
	select_color(liquidP2, Global.P2Color)


func _process(_delta) -> void:
	get_input()
	
	if resultP1 == null:
		calcul()
	
	labelP1.text = "%.1f" % (liquidP1.scale.x * 100)
	labelP2.text = "%.1f" % (abs(liquidP2.scale.x * 100))
	
	if liquidP1.scale.x < resultMin:
		liquidP1.scale.x += 0.01
		liquidP2.scale.x -= 0.01
	
	if liquidP1.scale.x >= resultMin:
		if timer > 50:
			if liquidP1.scale.x < pourcentP1:
				liquidP1.scale.x += 0.01
			if abs(liquidP2.scale.x) < pourcentP2:
				liquidP2.scale.x -= 0.01
				
		timer += 1	
	
	if liquidP1.scale.x >= pourcentP1 and abs(liquidP2.scale.x) >= pourcentP2:
		labelP1.text = "%.1f" % (pourcentP1 * 100)
		labelP2.text = "%.1f" % (pourcentP2 * 100)
		if pourcentP1 > pourcentP2 :
			label.text = "Player 1 Wins !"
		else:
			label.text = "Player 2 Wins !"
		
		if spaceP1 or spaceP2:
			var new = menuScene.instance()
			var game = get_tree().get_root().get_node("Game/CRT/Viewport/WindowGame")
	
			get_tree().get_root().get_node("Game/CRT/Viewport").add_child(new)
			Global.currentScene = "Menu"
			game.queue_free()
			queue_free()


func get_input() -> void:
	spaceP1 = Input.is_action_just_pressed("ui_select")
	spaceP2 = Input.is_action_just_pressed("ui_select_p2")


func calcul() -> void:
	resultP1 = Global.resultP1 + Global.bonusP1
	resultP2 = Global.resultP2 + Global.bonusP2
	resultP1Scale = resultP1 / 100
	resultP2Scale = - resultP2 / 100
	resultMin = min(resultP1Scale, abs(resultP2Scale))
	pourcentP1 = resultP1 / (resultP1 + resultP2)
	pourcentP2 = resultP2 / (resultP1 + resultP2)


func select_color(liquid, new_color) -> void:
	if new_color == "Red":
		liquid.material.set_shader_param("Color", Color("ff0000"))
	elif new_color == "Green":
		liquid.material.set_shader_param("Color", Color("00ff04"))
	elif new_color == "Blue":
		liquid.material.set_shader_param("Color", Color("0040ff"))
	elif new_color == "Yellow":
		liquid.material.set_shader_param("Color", Color("fffb00"))
