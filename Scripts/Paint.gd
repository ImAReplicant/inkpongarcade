extends Node2D

var ballPostion
var maxSizeInk = 4.0
var sizeInk = maxSizeInk
var sizeInkPowerShot = 8.0

# Colors
var redColor = Color("a6424c")
var greenColor = Color("99e550")
var blueColor = Color("507dcc")
var yellowColor = Color("eec39a")
var whiteColor = Color("ffffff")
var color = whiteColor
var colorP1
var colorP2

# Create a new sprite and texture that will save the old ink marks
var sprite = Sprite.new()
var texture = ImageTexture.new()

# An array to hold all the dictionaries that make up each ink mark.
var inkArray = []
const MAX_SIZE_ARRAY = 100 # It's low but seems to be a good value for the web


func _ready() -> void:
	Global.connect("game_is_finished", self, "inks_ratio")
	# Set sprite parameters
	sprite.centered = false
	sprite.show_behind_parent = true
	# Add Sprite node to the Node2D (parent)
	add_child(sprite, true)
	if Global.P1Color == "Red":
		colorP1 = redColor
	elif Global.P1Color == "Green":
		colorP1 = greenColor
	elif Global.P1Color == "Blue":
		colorP1 = blueColor
	elif Global.P1Color == "Yellow":
		colorP1 = yellowColor
		
	if Global.P2Color == "Red":
		colorP2 = redColor
	elif Global.P2Color == "Green":
		colorP2 = greenColor
	elif Global.P2Color == "Blue":
		colorP2 = blueColor
	elif Global.P2Color == "Yellow":
		colorP2 = yellowColor


func _process(_delta) -> void:
	if color != whiteColor:
		
		if sizeInk > maxSizeInk: 			
			sizeInk -= 0.02
			sizeInk = max(maxSizeInk, sizeInk)
			
		update()


func inks_ratio() -> void:
	var img = get_viewport().get_texture().get_data()
	img.lock()
	var img_size = img.get_size()
	var pixelP2 = 0
	var pixelP1 = 0
	var comptPixel = 0
	for x in range(0, img_size.x):
		for y in range(0, img_size.y):
			var pixel = img.get_pixel(x, y)
			comptPixel += 1
			if pixel == colorP2:
				pixelP2 += 1.0
			elif pixel == colorP1:
				pixelP1 += 1.0
	var ratioP2 = pixelP2 * 100 / comptPixel
	var ratioP1 = pixelP1 * 100 / comptPixel
	Global.resultP1 = ratioP1
	Global.resultP2 = ratioP2


func _draw() -> void:
	# Draw all ink marks
#	for ink in inkArray:
#		draw_circle(ink.ballPos, ink.inkSize, ink.inkColor)
	draw_circle(ballPostion, sizeInk, color)


func _on_Ball_change_color(new_color):
	color = new_color


func _on_Ball_change_position(pos):
	# Ball position correction
	var posCorrection = get_parent().get_parent().rect_position
	ballPostion = pos - posCorrection


func _on_PaddleP1_power_shot():
	sizeInk = sizeInkPowerShot


func _on_PaddleP2_power_shot():
	sizeInk = sizeInkPowerShot
