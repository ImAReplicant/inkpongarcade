extends Node2D


onready var timeLabel = $MarginContainer/VBoxContainer/VBoxContainer/HBoxTime/Label
onready var musicLabel = $MarginContainer/VBoxContainer/VBoxContainer/HBoxMusic/Label
onready var stageLabel = $MarginContainer/VBoxContainer/VBoxContainer/HBoxStage/Label
onready var startButton = get_parent().get_node("MarginContainer/VBoxContainer/HBoxStart_Settings/Start")
onready var musicStream = $Music
onready var music1 = load("res://Music/Chippy Music 1.wav")
onready var music2 = load("res://Music/Chippy Music 2.wav")
onready var music3 = load("res://Music/Chippy Music 3.wav")

var time = Global.timer
var music = Global.musicPlay
var stage = Global.stage

var timeFocus = false
var stageFocus = false
var musicFocus = false
var can_change = true


func _ready() -> void:
	timeLabel.text = "%s s" % time
	stageLabel.text = "Stage %s" % stage
	musicLabel.text = "Music %s" % music


func _process(_delta) -> void:
	if timeFocus:
		timeLabel.text = "< %s s >" % time
		
		if get_input("right") and can_change:
			time += 1
			timer()
		if get_input("left") and can_change:
			time -= 1
			timer()

	if stageFocus:
		stageLabel.text = "< Stage %s >" % stage
		
		if get_input("right") and can_change:
			stage += 1
			timer()
		if get_input("left") and can_change:
			stage -= 1
			timer()

	if musicFocus:
		musicLabel.text = "< Music %s >" % music
		
		if get_input("right") and can_change:
			music += 1
			timer()
		if get_input("left") and can_change:
			music -= 1
			timer()
			
		if music == 1 and musicStream.stream != music1:
			musicStream.stream = music1
			if not musicStream.is_playing():
				musicStream.play()
		elif music == 2 and musicStream.stream != music2:
			musicStream.stream = music2
			if not musicStream.is_playing():
				musicStream.play()
		elif music == 3 and musicStream.stream != music3:
			musicStream.stream = music3
			if not musicStream.is_playing():
				musicStream.play()

	time = clamp(time, 30, 120)
	music = clamp(music, 1, 3)
	stage = clamp(stage, 1, 4)
	Global.timer = time
	Global.musicPlay = music
	Global.stage = stage


func timer():
	can_change = false
	yield(get_tree().create_timer(0.2), "timeout")
	can_change = true


func get_input(key) -> bool:
	if key == "right":
		return Input.is_action_pressed("ui_right")
	elif key == "left":
		return Input.is_action_pressed("ui_left")
	return false


func _on_Time_focus_entered() -> void:
	timeFocus = true


func _on_Time_focus_exited() -> void:
	timeLabel.text = "%s s" % time
	timeFocus = false


func _on_Stage_focus_entered():
	stageFocus = true


func _on_Stage_focus_exited():
	stageLabel.text = "Stage %s" % stage
	stageFocus = false


func _on_Music_focus_entered():
	musicFocus = true
	if not musicStream.is_playing():
		musicStream.play()


func _on_Music_focus_exited():
	musicLabel.text = "Music %s" % music
	musicFocus = false
	if musicStream.is_playing():
		musicStream.stop()


func _on_Back_pressed():
	visible = false
	startButton.grab_focus()
