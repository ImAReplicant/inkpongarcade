extends KinematicBody2D

signal power_shot

export(String) var color = "Blue"
enum PLAYER { P2, CPU }
export(PLAYER) var player = PLAYER.CPU

const SPEED = 200
var direction = Vector2()
var lockPositionX = 0
var maxLiquid = 0.80
var minLiquid = 0.43
var liquidFill = maxLiquid
var liquidCostPowerShot = 0.1

var rollADice = true
var random
var oldPositionBall

var down = 0
var up = 0
var reload
var powerShot = 0
var CPUshot = 0

onready var powerShotParticles = $PowerShotParticles
onready var powerShotSound = $PowerShotSound
onready var timer = $Timer
onready var liquid = $Sprite/Liquid
onready var ball = get_node("../Ball")

onready var tree = get_tree().get_root().get_node("Game")
onready var lightRight = tree.get_node("Light2DRight").get_node("AnimationPlayer")


func _ready() -> void:
	randomize()
	oldPositionBall = ball.position
	# Locks the X position to prevent shifting when the ball hits the paddle
	lockPositionX = position.x
	if Global.game == "CPU":
		player = PLAYER.CPU
	elif Global.game == "P2":
		player = PLAYER.P2
	select_color(Global.P2Color)


func select_color(new_color) -> void:
	if new_color == "Red":
		liquid.material.set_shader_param("Color", Color("ff0000"))
		$CPUParticles2D.color = Color("a6424c")
		powerShotParticles.color = Color("a6424c")
	elif new_color == "Green":
		liquid.material.set_shader_param("Color", Color("00ff04"))
		$CPUParticles2D.color = Color("99e550")
		powerShotParticles.color = Color("99e550")
	elif new_color == "Blue":
		liquid.material.set_shader_param("Color", Color("0040ff"))
		$CPUParticles2D.color = Color("507dcc")
		powerShotParticles.color = Color("507dcc")
	elif new_color == "Yellow":
		liquid.material.set_shader_param("Color", Color("fffb00"))
		$CPUParticles2D.color = Color("eec39a")
		powerShotParticles.color = Color("eec39a")


func get_input() -> void:
	paddle_player(player)
	if liquidFill <= minLiquid:
		direction = Vector2.ZERO
	else:
		direction = Vector2(0, down - up)
	if direction == Vector2.ZERO:
		$CPUParticles2D.emitting = false
	else:
		$CPUParticles2D.emitting = true
	
	if reload:
		liquidFill += 0.001
		direction = Vector2.ZERO


func _physics_process(delta) -> void:
	get_input()
	position.x = lockPositionX
	var collision = move_and_collide(SPEED * direction * delta)
	
	if collision:
		if collision.collider.name == "Ball":
			collision()
	
	liquidFill = liquid_fill(collision)
	position.y = round(position.y)
	
	if timer.time_left > 0:
		$Sprite.modulate = Color(10,10,10,10)
		
		if powerShot:
			if liquidFill >= minLiquid + liquidCostPowerShot:
				powerShot = 0
				get_tree().paused = true
				powerShotSound.play()
				powerShotParticles.emitting = true
				liquidFill -= liquidCostPowerShot
				emit_signal("power_shot")


func liquid_fill(col) -> float:
	if direction != Vector2.ZERO and !col:
		liquidFill -= 0.001
	liquid.material.set_shader_param("Fill", liquidFill)
	return clamp(liquidFill, minLiquid, maxLiquid)


func paddle_player(play) -> void:
	if play == PLAYER.P2:
		down = Input.get_action_strength("ui_down_p2")
		up = Input.get_action_strength("ui_up_p2")
		reload = Input.is_action_pressed("ui_select_p2")
		powerShot = Input.is_action_just_pressed("ui_select_p2")
	if play == PLAYER.CPU:
		var difference = ball.position - position
		down = 0
		up = 0
		
		if oldPositionBall.x < ball.position.x and ball.position.x > 100:
			if -5 <= difference.y and difference.y <= 5:
				roll_a_dice(5)
				if random == 0:
					down = 1
				else:
					up = 1

			roll_a_dice(35, 1)
			if 6 < difference.y and difference.y > random:
				down = 1
			if -6 > difference.y and difference.y < - random:
				up = 1
			
		elif oldPositionBall.x > ball.position.x:
			roll_a_dice(4)
			if random == 0:
				CPUshot = 0
		
		if (liquidFill < 0.6 and ball.color == $CPUParticles2D.color) or (liquidFill < rand_range(0.7, 1) and oldPositionBall.x > ball.position.x) :
			reload = 1
		else:
			reload = 0
			
		oldPositionBall = ball.position


func collision() -> void:
	lightRight.play("Blink")
			
	if timer.is_stopped():
		timer.start(0.090)
		
		if player == PLAYER.CPU and liquidFill > 0.6 and CPUshot == 0:
				CPUshot = 1
				powerShot = 1
	
	rollADice = true


func roll_a_dice(maximum, minimum = 0) -> void:
	if rollADice == true:
		random = randi() % maximum + minimum
		rollADice = false


func _on_Timer_timeout():
	get_tree().paused = false
	$Sprite.modulate = Color(1,1,1,1)
